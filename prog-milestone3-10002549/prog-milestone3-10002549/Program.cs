﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10002549
{
    class Program
    {
        //Main Menu
        static void Menu()
        {
            Console.Clear();

            Console.WriteLine("Hello! Welcome to PRIMO PIZZA'S we are looking forward cooking for you - the best Pizza's you have ever tried!\n");
            Console.WriteLine("\nWe recommend you begin with entering your Customer Details so that they can be matched with your order.\n");
            Console.WriteLine("\n                                   Please select from the Menu below.\n");
            Console.WriteLine("************************************************************************************************************");
            Console.WriteLine("*                                               MAIN MENU                                                  *");
            Console.WriteLine("*                                                                                                          *");
            Console.WriteLine("*                                           1. Enter Customer Details                                      *");
            Console.WriteLine("*                                           2. Order Pizza                                                 *");
            Console.WriteLine("*                                           3. Order Drinks                                                *");
            Console.WriteLine("*                                           4. Check Your Order                                            *");
            Console.WriteLine("*                                           5. Cancel Your Order                                           *");
            Console.WriteLine("*                                           6. Confirm Order & Pay                                         *");
            Console.WriteLine("*                                                                                                          *");
            Console.WriteLine("************************************************************************************************************");
        }

        //Menu Item 1. Enter Customer Details
        static void Customer()
        {
            var firstname = "";
            var surname = "";
            var phonenumber = "";

            var CustomerID = new List<Tuple<string, string, string>>();

            Console.WriteLine("\nPlease enter your name and phone number so that we can match these with your order.");

            Console.Write("\nPlease enter your First Name: ");
            firstname = (Console.ReadLine());
            Console.Write("\nPlease enter your Surname: ");
            surname = Console.ReadLine();
            Console.Write("\nPlease enter your Phone Number: ");
            phonenumber = (Console.ReadLine());

            CustomerID.Add(Tuple.Create(firstname, surname, phonenumber));
            
            bool InValid = true;
            do
            {
                Console.Clear();
                Console.WriteLine("\nThank You!\n");
                Console.WriteLine(string.Join("\n,", CustomerID));
                Console.Write("\nWould you like to place an Order? Type in 'yes' to go to the Main Menu to place your order: \n");
                Console.Write("\nIf you do not want to place an Order type in 'no' to exit: ");

                var input = Console.ReadLine().ToLower();

                if (input == "yes")
                {
                    Menu();
                    InValid = false;
                }
                else if (input == "no")
                {
                    Environment.Exit(0);
                    InValid = false;
                }
                if (InValid)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (InValid);
        }

        //Menu Item 2. Order Pizza
        static void ChoosePizza(List<Tuple<string, string, string>> CustomerID, List<Tuple<string, double>> Drinks)
        {
            var Order = new List<Tuple<string, string, double>>();

            bool Condition = true;
            do
            {
                Console.WriteLine("Please select your Pizza from our Menu of Delicious Pizza's");
                Console.WriteLine("\n1. Hawian Pizza: ");
                Console.WriteLine("\n2. BBQ Chicken Pizza: ");
                Console.WriteLine("\n3. Pepperoni Pizza: ");
                Console.WriteLine("\n4. Margarita Pizza: ");
                Console.WriteLine("\n5. Meat Lovers Pizza: ");
                Console.WriteLine("\n6. Seafood Pizza: ");
                Console.WriteLine("\n7. Vegetarian Pizza: ");

                Console.Write("\nEnter the Number for your Pizza Selection: ");

                var PizzaSelect = Console.ReadLine();

                switch (PizzaSelect)
                {
                    case "1":
                        PizzaSelect = "Hawaiian";
                        break;

                    case "2":
                        PizzaSelect = "BBQ Chicken";
                        break;

                    case "3":
                        PizzaSelect = "Pepperoni";
                        break;

                    case "4":
                        PizzaSelect = "Margarita";
                        break;

                    case "5":
                        PizzaSelect = "Meat Lovers";
                        break;

                    case "6":
                        PizzaSelect = "Seafood";
                        break;

                    case "7":
                        PizzaSelect = "Vegetarian";
                        break;

                    default:
                        Console.WriteLine("Error! Please try again");
                        break;
                }

                Console.Clear();
                Console.WriteLine("\nPlease select from the 3 Pizza Sizes");
                Console.WriteLine("\n1. Small - 7inch / 18cms: Price $12.50");
                Console.WriteLine("\n2. Medium - 12inch / 31cms: Price = $16.50");
                Console.WriteLine("\n3. Large - 16inch / 36cms: Price = $20.50");

                Console.Write("\nPlease enter the number for your Pizza Size selection: ");

                //Select Pizza Size
                var PizzaSize = Console.ReadLine();
                double PizzaPrice = 0;

                switch (PizzaSize)
                {
                    case "1":
                        PizzaSize = "Small";
                        break;

                    case "2":
                        PizzaSize = "Medium";
                        break;

                    case "3":
                        PizzaSize = "Large";
                        break;

                    default:
                        Console.WriteLine("Error! Please try again.");
                        break;
                }

                //Pizza Prices
                if (PizzaSize == "Small")
                {
                    PizzaPrice = 12.50;
                }
                else if (PizzaSize == "Medium")
                {
                    PizzaPrice = 16.50;
                }
                else if (PizzaSize == "Large")
                {
                    PizzaPrice = 20.50;
                }

                Order.Add(Tuple.Create(PizzaSelect, PizzaSize, PizzaPrice));

                Console.Clear();
                Console.WriteLine("\nThank You! Here is your most excellent Pizza selection.\n");
                for (int i = 0; i < Order.Count; i++)
                {
                    Console.WriteLine($"{i} = {Order[i]}");
                }

                //Select another Pizza or go back to menu
                Console.WriteLine("\nWould you like to order another Pizza?");
                Console.Write("\nType 'yes' to select another Pizza from the Menu");
                Console.Write("\nType in 'no' to return to the Main Menu to complete your order");

                var input = Console.ReadLine().ToLower();

                if (input == "yes")
                {
                    Condition = true;
                }

                else if (input == "no")
                {
                    Menu();
                    Condition = false;
                }
                if (Condition)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (Condition);
        }

        // Menu Item 3. Order Drinks
        static void ChooseDrinks(List<Tuple<string, string, string>> CustomerID, List<Tuple<string, string, double>> Order)
        {
            var Drinks = new List<Tuple<string, double>>();
            
            bool Condition = true;
            do
            {
                Console.WriteLine("Please select a Thirst Quenching Drink to go with your Delicious Pizza's");
                Console.WriteLine("\n1. CocaCola: ");
                Console.WriteLine("\n2. FreshUp: ");
                Console.WriteLine("\n3. IceTea: ");
                Console.WriteLine("\n4. Sprite: ");
                Console.WriteLine("\n5. Lift: ");
                Console.WriteLine("\n6. JustJuice: ");
                Console.WriteLine("\n7. Fanta: ");

                Console.Write("\nEnter the Number for your Drink Selection: ");

                string DrinkSelect = Console.ReadLine();

                switch (DrinkSelect)
                {
                    case "1":
                        DrinkSelect = "CocaCola: $3.50";
                        break;

                    case "2":
                        DrinkSelect = "FreshUp: $5.50";
                        break;

                    case "3":
                        DrinkSelect = "IceTea: $6.50";
                        break;

                    case "4":
                        DrinkSelect = "Sprite: $3.75";
                        break;

                    case "5":
                        DrinkSelect = "Lift: $3.85";
                        break;

                    case "6":
                        DrinkSelect = "JustJuice: $5.85";
                        break;

                    case "7":
                        DrinkSelect = "Fanta: $3.65";
                        break;

                    default:
                        Console.WriteLine("Error! Please try again");
                        break;
                }

                //Drinks Prices
                double DrinkPrice = 0;

                if (DrinkSelect == "CocaCola")
                {
                    DrinkPrice = 3.50;
                }
                else if (DrinkSelect == "FreshUp")
                {
                    DrinkPrice = 5.50;
                }
                else if (DrinkSelect == "IceTea")
                {
                    DrinkPrice = 6.50;
                }
                else if (DrinkSelect == "Sprite")
                {
                    DrinkPrice = 3.75;
                }
                else if (DrinkSelect == "Lift")
                {
                    DrinkPrice = 3.85;
                }
                else if (DrinkSelect == "JustJuice")
                {
                    DrinkPrice = 5.85;
                }
                else if (DrinkSelect == "Fanta")
                {
                    DrinkPrice = 3.65;
                }

                Drinks.Add(Tuple.Create(DrinkSelect, DrinkPrice));

                Console.Clear();
                Console.WriteLine("\nThank You! Here is your refreshing Drinks selection.\n");

                //Order more drinks or go back to the Main Menu
                for (int i = 0; i < Drinks.Count; i++)
                {
                    Console.WriteLine($"{i} = {Drinks[i]}");
                }

                Console.WriteLine("\nWould you like to order another Drink?");
                Console.Write("\nType 'yes' to select another Drink from the Menu");
                Console.Write("\nType in 'no' to return to the Main Menu to complete your order");

                var input = Console.ReadLine().ToLower();

                if (input == "yes")
                {
                    Condition = true;
                }

                else if (input == "no")
                {
                    Menu();
                    Condition = false;
                }
                if (Condition)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (Condition);

        CheckOrder(CustomerID, Order, Drinks);
        }
        
        // Menu Item 4. Check Order
        static void CheckOrder(List<Tuple<string, string, string>> CustomerID, List<Tuple<string, string, double>> Order, List<Tuple<string, double>> Drinks)        
        {
            Console.WriteLine(string.Join(",", CustomerID));
            Console.WriteLine(string.Join(",", Order));
            Console.WriteLine(string.Join(",", Drinks));

            bool InValid = true;
            do
            {
                Console.Write("\nType in 'yes' to return to the Main Menu and complete your order.\n");
                Console.Write("\nIf you are sure you want to cancel your order type in 'yes' to exit: ");

                var input = Console.ReadLine().ToLower();

                if (input == "no")
                {
                    Menu();
                    InValid = false;
                }
                else if (input == "yes")
                {
                    Environment.Exit(0);
                    InValid = false;
                }
                if (InValid)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (InValid);

        ConfirmAndPay(CustomerID, Order, Drinks);
        }
        
        // Menu Item 5. Cancel Order
        static void CancelOrder()
        {
            Console.WriteLine("\nWe are sorry you have decided not proceed with your Pizza order!");
            Console.WriteLine("\nYou are really missing out on a special treat, however we are sure you have your reasons.");
            Console.WriteLine("\nWe hope to have the pleasure of serving you again soon.");

            bool InValid = true;
            do
            {
                Console.Write("\nType in 'no' if you made a mistake and would like to return to the Main Menu \n");
                Console.Write("\nIf you are sure you want to cancel your order type in 'yes' to exit: ");

                var input = Console.ReadLine().ToLower();

                if (input == "no")
                {
                    Menu();
                    InValid = false;
                }
                else if (input == "yes")
                {
                    Environment.Exit(0);
                    InValid = false;
                }
                if (InValid)
                {
                    Console.Clear();
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
            } while (InValid);
        }

        // Menu Item 6. Confirm Order and Pay
        static void ConfirmAndPay(List<Tuple<string, string, string>> CustomerID, List<Tuple<string, string, double>> Order, List<Tuple<string, double>> Drinks)
        {
            DateTime today = DateTime.Today;
            Console.WriteLine($"\nThankyou for your Pizza order on {today}");

            double SumPizza = 0;

            foreach (var value in Order)
            {
                SumPizza += value.Item2[0];
            }
            Console.WriteLine($"\nThe total amount for your Pizza order is: ${SumPizza}");
            double SumDrinks = 0;

            foreach (var value in CustomerID)
            {
                SumDrinks += value.Item2[0];
            }
            Console.WriteLine($"\nThe total amount for your Drinks order is: ${SumDrinks}");

            double Total = (SumDrinks + SumPizza);
            Console.WriteLine($"\nThe total to pay for your Pizza and Drinks is: ${Total}");

            Console.Write($"\nPlease enter the cash amount in dollars and cents that you will be paying with: $");
            double Cash = int.Parse(Console.ReadLine());

            double Change = (Cash - Total);
            Console.WriteLine($"Thank you for your payment of ${Cash} your total order value was ${Total}\nTherefore the change owing to you is ${Change}");

            Console.WriteLine("\nThank you for Choosing to Dine with us. We hope you are happy with your Pizza's \nand that we will have the pleasure seeing you again soon.");

            ConfirmAndPay(CustomerID, Order, Drinks);
        }
        static void GoToMenu(List<int> list)
        {
        }
    

        static void Main(string[] args)
        {
            List<int> list = new List<int>();

            while (true)
            {
                Menu();

                Console.Write("Please enter your Menu Selection from 1 - 7: ");
                var userinput = Console.ReadLine().ToLower();
                int i;
                bool IsValid = int.TryParse(userinput, out i);
                bool IsEmpty = string.IsNullOrWhiteSpace(userinput);

                if (userinput == "no")
                {
                    Environment.Exit(0);
                    break;
                }

                if (!IsValid || i > 7 || IsEmpty)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid Input! Please Enter a Valid Number!");
                    Console.ReadKey();
                    userinput = string.Empty;
                }

                else
                {
                    switch (i)
                    {
                        case 1:
                            Console.Clear();

                            Customer();

                            GoToMenu(list);
                            break;

                        case 2:
                            Console.Clear();

                            ChoosePizza();

                            GoToMenu(list);
                            break;

                        case 3:
                            Console.Clear();

                            ChooseDrinks();

                            GoToMenu(list);
                            break;

                        case 4:
                            Console.Clear();

                            CheckOrder();

                            GoToMenu(list);
                            break;

                        case 5:
                            Console.Clear();

                            CancelOrder();

                            GoToMenu(list);
                            break;

                        case 6:
                            Console.Clear();

                            ConfirmAndPay();

                            GoToMenu(list);
                            break;
                    }
                }
            }          
            Console.ReadLine();
        }
     }
}


